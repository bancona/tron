# Tron Game

#### By Alberto (Bertie) Ancona
#### Developed in spring 2013

## Overview
Tron is one of a series of arcade games I built for fun during my junior year of high school. It was my first attempt at a multiplayer game, and I set it up such that the two players would share a single keyboard, one using WASD and the other using the arrow keys. One of the challenges I faced was that as the game went on and the number of walls in the field increased, checking for collisions between the players and the walls slowed the game significantly because I checked every game step. I solved this issue by predicting collisions every time a player made a turn and storing the earliest time computed. That way, I only checked for collisions as the players changed direction instead of dozens of times per second. This resulted in a large performance boost and eliminated any noticeable lag in the game.

## Setup Instructions
I have included a runnable jar file that will start the game without any setup whatsoever. If you prefer, you can compile the Java files yourself by running the command `javac Tron.java` and then run the program by running the command `java Tron` from the [src](./src) directory.