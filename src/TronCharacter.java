import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.awt.Component;

public class TronCharacter extends Component implements KeyListener {
	private static final long serialVersionUID = 8781429603860489846L;
	// Individual TronCharacter Attributes:
	private int x, y, direction, colCountdown = Integer.MAX_VALUE;
	private final int START_X, START_Y, START_DIR, WIDTH = 3, HEIGHT = 3;
	public Path myPath = new Path();
	public boolean myCollision;
	public Color myColor;
	// All characters in game
	public static ArrayList<TronCharacter> tronCharacters = new ArrayList<TronCharacter>();
	public static boolean isTie;

	// CONTROLS:
	// References player's specific set of directional keys
	private final int[] myControls;
	// Player 1 uses up, down, left, right arrow keys
	public static final int[] player1Controls = { KeyEvent.VK_UP,
			KeyEvent.VK_DOWN, KeyEvent.VK_RIGHT, KeyEvent.VK_LEFT };
	// Player 2 uses W, S, A, D keys
	public static final int[] player2Controls = { KeyEvent.VK_W, KeyEvent.VK_S,
			KeyEvent.VK_D, KeyEvent.VK_A };
	// Constants used to reference elements of myControls
	public static final int UP = 0, DOWN = 1, RIGHT = 2, LEFT = 3;

	public TronCharacter(Color c, int x, int y, int player, int dir) {
		myColor = c;
		START_X = this.x = x;
		START_Y = this.y = y;

		if (player == 2) {
			myControls = player2Controls;
		} else {
			// player1Controls are the default directional keys
			myControls = player1Controls;
		}
		if (dir >= 0 && dir <= 3) {
			direction = myControls[dir];
		}
		START_DIR = direction;
		myPath.addSeg(this.x, this.y);
		tronCharacters.add(this);
	}

	@Override
	public void paint(Graphics g) {
		myPath.paint(g, myColor);
		g.setColor(myColor);
		g.fillRect(x - WIDTH / 2 - 1, y - HEIGHT / 2 - 1, WIDTH + 2, HEIGHT + 2);
		if (myCollision) {
			g.setColor(new Color(myColor.getRed(), myColor.getGreen(), myColor
					.getBlue(), 100));
			g.fillOval(x - WIDTH / 2 - 6, y - HEIGHT / 2 - 6, 15, 15);
		}
	}

	public void act() {
		if (colCountdown <= 0 || collisionImminent()) {
			Tron.dead = true;
			myCollision = true;
			repaint();
		} else {
			move();
		}
		repaint();
		colCountdown--;
	}

	private void move() {
		x = getNextPoint().x;
		y = getNextPoint().y;
		myPath.addPoint(x, y);
	}

	private Point getNextPoint() {
		int nextX = x, nextY = y;
		if (direction == myControls[UP]) {
			nextY--;
		} else if (direction == myControls[DOWN]) {
			nextY++;
		} else if (direction == myControls[RIGHT]) {
			nextX++;
		} else if (direction == myControls[LEFT]) {
			nextX--;
		}
		return new Point(nextX, nextY);
	}

	private boolean collisionImminent() {
		// if hits edge of game
		if (!(new Rectangle(0, 0, Tron.WIDTH, Tron.HEIGHT))
				.contains(getNextPoint())) {
			return true;
		}
		for (TronCharacter tc : tronCharacters) {
			if (!tc.equals(this)
					&& getCollisionArea().intersects(
							tc.myPath.getSegs().get(
									tc.myPath.getSegs().size() - 1))) {
				if (getCollisionArea().intersects(tc.getCollisionArea())) {
					isTie = true;
				}
				return true;
			}
		}
		return false;
	}

	private int distToCollision() {
		Rectangle futurePath = getFuturePath();
		int minTimeToCol = Integer.MAX_VALUE;
		for (TronCharacter tc : tronCharacters) {
			for (int i = 0; i < tc.myPath.getSegs().size() - 1; i++) {
				Rectangle futureCollisionArea = futurePath
						.intersection(tc.myPath.getSegs().get(i));
				if (!futureCollisionArea.isEmpty()) {
					int timeToCol = getShortestDist(
							getCenter(getCollisionArea()), futureCollisionArea);
					if (timeToCol >= 0 && timeToCol < minTimeToCol) {
						minTimeToCol = timeToCol;
					}
				}
			}
		}
		return minTimeToCol;
	}

	private Rectangle getFuturePath() {
		Rectangle futurePath = getCollisionArea();
		Point endPt = getCenter(futurePath);
		if (direction == myControls[UP]) {
			endPt.y = 0;
		} else if (direction == myControls[DOWN]) {
			endPt.y = Tron.HEIGHT;
		} else if (direction == myControls[RIGHT]) {
			endPt.x = Tron.WIDTH;
		} else if (direction == myControls[LEFT]) {
			endPt.x = 0;
		}
		futurePath.add(endPt);
		return futurePath;
	}

	private Rectangle getCollisionArea() {
		return new Rectangle(x - WIDTH / 2, y - HEIGHT / 2, WIDTH, HEIGHT);
	}

	private Point getCenter(Rectangle rect) {
		return new Point((int) rect.getCenterX(), (int) rect.getCenterY());
	}

	private int getShortestDist(Point pt, Rectangle rect) {
		int lowestDist = Integer.MAX_VALUE;
		// distance to top of rectangle
		Point top = new Point((int) (rect.getCenterX()),
				(int) (rect.getCenterY() - rect.height / 2));
		if (pt.distance(top) < lowestDist) {
			lowestDist = (int) pt.distance(top);
		}
		// distance to bottom of rectangle
		Point bot = new Point((int) (rect.getCenterX()),
				(int) (rect.getCenterY() + rect.height / 2));
		if (pt.distance(bot) < lowestDist) {
			lowestDist = (int) pt.distance(bot);
		}
		// distance to left of rectangle
		Point lft = new Point((int) (rect.getCenterX() - rect.width / 2),
				(int) (rect.getCenterY()));
		if (pt.distance(lft) < lowestDist) {
			lowestDist = (int) pt.distance(lft);
		}
		// distance to right of rectangle
		Point rgt = new Point((int) (rect.getCenterX() + rect.width / 2),
				(int) (rect.getCenterY()));
		if (pt.distance(rgt) < lowestDist) {
			lowestDist = (int) pt.distance(rgt);
		}
		return lowestDist;
	}

	public static ArrayList<TronCharacter> getCharacters() {
		return tronCharacters;
	}

	public void reset() {
		direction = START_DIR;
		x = START_X;
		y = START_Y;
		myPath.reset(x, y);
		colCountdown = Integer.MAX_VALUE;
		myCollision = false;
		isTie = false;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (Tron.paused || Tron.dead) {
			return;
		}
		int key = e.getKeyCode();
		// if valid key and making a turn, change direction
		if (key != direction
				&& ((key == myControls[UP] && direction != myControls[DOWN])
						|| (key == myControls[DOWN] && direction != myControls[UP])
						|| (key == myControls[RIGHT] && direction != myControls[LEFT]) || (key == myControls[LEFT] && direction != myControls[RIGHT]))) {
			direction = key;

			// so that when turning does not hit itself
			x = x - WIDTH * (x - getNextPoint().x);
			y = y - HEIGHT * (y - getNextPoint().y);
			// path adds new segment when character turns
			myPath.addSeg(x, y);
			for (TronCharacter tc : tronCharacters) {
				tc.colCountdown = tc.distToCollision();
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}
