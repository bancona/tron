import java.awt.Graphics;
import java.awt.Color;
import java.awt.Rectangle;
import java.util.ArrayList;

public class Path {
	private ArrayList<Rectangle> segs = new ArrayList<Rectangle>();
	private Rectangle currSeg;
	private final int THICKNESS = 3;

	public void paint(Graphics g, Color c) {
		g.setColor(c);
		for (Rectangle seg : segs) {
			g.fillRect(seg.x, seg.y, seg.width, seg.height);
		}
	}

	public void addPoint(int x, int y) {
		currSeg.add(x, y);
	}

	public void addSeg(int x, int y) {
		// fixes corners
		if (currSeg != null) {
			if (currSeg.width > currSeg.height) {
				currSeg.grow(1, 0);
			} else {
				currSeg.grow(0, 1);
			}
		}
		segs.add(currSeg = new Rectangle(x - 1, y - 1, THICKNESS, THICKNESS));
	}

	public void reset(int x, int y) {
		segs.clear();
		addSeg(x, y);
	}

	public ArrayList<Rectangle> getSegs() {
		return segs;
	}

}
