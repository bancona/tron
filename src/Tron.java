import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JFrame;

public class Tron extends JFrame implements MouseListener {
	private static final long serialVersionUID = -3590670147502592880L;
	public static boolean paused, dead;
	public static final int WIDTH = 600;
	public static final int HEIGHT = 400;

	public Tron(String s) {
		super(s);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		getContentPane().setPreferredSize(new Dimension(WIDTH, HEIGHT));
		getContentPane().setBackground(Color.BLACK);
		pack();
		setVisible(true);
		addMouseListener(this);
		// add red character
		TronCharacter tc = new TronCharacter(Color.RED, WIDTH - 100,
				HEIGHT / 2, 1, TronCharacter.LEFT);
		add(tc);
		validate();
		addKeyListener(tc);
		// add blue character
		tc = new TronCharacter(Color.CYAN, 100, HEIGHT / 2, 2,
				TronCharacter.RIGHT);
		add(tc);
		validate();
		addKeyListener(tc);
	}

	public static void main(String[] args) {
		Tron jf = new Tron("Tron");
		jf.play();
	}

	public void play() {
		while (true) {
			repaint();
			paused = true;
			pause(1000);
			paused = false;
			while (!dead) {
				pause(5);
				for (TronCharacter tc : TronCharacter.getCharacters()) {
					tc.act();
				}
			}
			repaint();
			while (dead) {
				pause(1);
			}
			for (TronCharacter tc : TronCharacter.getCharacters()) {
				tc.reset();
			}
		}
	}

	public void pause(int ms) {
		try {
			Thread.sleep(ms);
		} catch (Exception e) {
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		// prints in bold "CLICK TO RESTART" on screen in white with a black
		// border
		if (dead) {
			g.setColor(Color.BLACK);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 53, HEIGHT / 2 - 50);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 54, HEIGHT / 2 - 51);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 55, HEIGHT / 2 - 49);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 56, HEIGHT / 2 - 50);
			g.setColor(Color.WHITE);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 54, HEIGHT / 2 - 50);
			g.drawString("CLICK TO RESTART", WIDTH / 2 - 55, HEIGHT / 2 - 50);
			for (TronCharacter tc : TronCharacter.tronCharacters) {
				if (!tc.myCollision) {
					String colorWon = "";
					Color displayColor = tc.myColor;
					if (tc.myColor.equals(Color.RED)) {
						colorWon = "RED WON!";
					} else if (tc.myColor.equals(Color.CYAN)) {
						colorWon = "BLUE WON!";
					}
					if (TronCharacter.isTie) {
						colorWon = "IT'S A TIE!";
						displayColor = Color.WHITE;
					}
					g.setColor(Color.BLACK);
					g.drawString(colorWon, WIDTH / 2 - 30, HEIGHT / 2 - 70);
					g.drawString(colorWon, WIDTH / 2 - 31, HEIGHT / 2 - 71);
					g.drawString(colorWon, WIDTH / 2 - 32, HEIGHT / 2 - 69);
					g.drawString(colorWon, WIDTH / 2 - 33, HEIGHT / 2 - 70);
					g.setColor(displayColor);
					g.drawString(colorWon, WIDTH / 2 - 31, HEIGHT / 2 - 70);
					g.drawString(colorWon, WIDTH / 2 - 32, HEIGHT / 2 - 70);
					return;
				}
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		dead = false;
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

}
